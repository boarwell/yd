# yd - Yatta! Dekitazo!!

プロコンのテストプログラム

## 使い方

### 1. `in + ファイル名`と`out + ファイル名`という名前のファイルを用意する

ファイル名が`a.go`なら`ina`に入力、`outa`に期待する出力を記述する

例: https://beta.atcoder.jp/contests/abc080/tasks/abc080_b

```sh

$ cat ina
12

$ cat outa
Yes

```

### 2． 実行

```sh

$ yd
SUCCESS: a.go

# 失敗したときは
$ yd
FAILED: a.go -> Expected: 1000 Acutually: 100

```

## TODO

- [ ] 複数のケースに対応する
- [ ] 指定したファイルだけ実行する
- [ ] 入出力用のディレクトリを作る
  - `in*`, `out*`ファイルは別のディレクトリに置きたい
- [ ] Ruby, Go両方のコードを実行してほしい
